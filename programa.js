var turno="X";

function realizarJugada(boton){


    boton.value=turno;

    if(turno=="X"){
        turno="O";
    }else{
        turno="X";
    }

}

function crearTablero() {
  let miBoton;
  let formulario;
  let i;

  formulario = document.getElementById("tablero");

  for ( i = 1; i < 10; i++) {
    miBoton = document.createElement("input");
    miBoton.setAttribute("type", "button");
    miBoton.setAttribute("class", "casilla");
    miBoton.setAttribute("value", " ");
    miBoton.setAttribute("id", "boton"+i);
    miBoton.setAttribute("onclick", "realizarJugada(this)");


    formulario.appendChild(miBoton);
    if(i%3==0){
        let salto = document.createElement("br");
        formulario.appendChild(salto);

    }
    
  }
}
